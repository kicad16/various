# Various

## About
This repository contains a KiCAD library with various components. Different types of components and different manufacturers. These were used by me for various private and commercial projects. You are welcome to use these in any way you like.

## Licensing
Available under the MIT license. No warranty whatsoever.

## User manual
Add the schematics symbol and footprint library to your list of libraries. Then configure the path (Preferences -> Configure Paths...) for *KICAD_VARIOUS_LIB_DIR* to the location of the Various library folder, and you will see the 3D models.

## Content
| Manufacturer | Part number | Description | Schematic symbol | Comments | Datasheet |
| ------------ | ------------ | ------------ | ------------ | ------------ | ------------ |
| Aosong | DHT22 | Temperature and humidity sensor | KiCAD default DHT11 | [3D CAD file from How To Mechatronics](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/ "3D CAD file from How To Mechatronics") | [Aosong official](http://www.aosong.com/en/products-22.html "Link to Aosong official website")
Arduino | Pro Mini | Small Microchip ATmega328P based Arduino board | Included in Various schematic symbol library | [3D CAD file from How To Mechatronics](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/ "3D CAD file from How To Mechatronics") | [Arduino official](https://docs.arduino.cc/retired/boards/arduino-pro-mini "Link to Arduino official website")
Multicomp Pro | MP-K78xx-1000R3 | DC/DC Converter | Any KiCad default L780x | L780x drop in replacement with high efficiency | [Farnell](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/https://www.farnell.com/datasheets/3167211.pdf "Datasheet on the Farnell websie")
Winsen | MH-Z19B | CO2 sensor | Included in Various schematic symbol library | *Footprint not 100% finished.* [3D CAD file from How To Mechatronics](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/ "3D CAD file from How To Mechatronics") | [Winsen Sensor official](https://www.winsen-sensor.com/sensors/co2-sensor/mh-z19b.html "Link to Winsen Sensor official website")
| Winsen | MP503 | Air-Quality Gas Sensor | Included in Various schematic symbol library | [3D CAD file from How To Mechatronics](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/ "3D CAD file from How To Mechatronics") | [Winsen Sensor official](https://www.winsen-sensor.com/sensors/voc-sensor/mp503.html "Link to Winsen Sensor official website")
| Winsen | MQ-131 | Ozon / O3 sensor | KiCAD default MQ-6 | [3D CAD file from How To Mechatronics](https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/ "3D CAD file from How To Mechatronics") | [Winsen Sensor official](https://www.winsen-sensor.com/sensors/o3-gas-sensor/mq131-l.html "Link to Winsen Sensor official website")
